+++
# Date this page was created.
date = "2017-01-01"

# Project title.
title = "Site Internet"

# Project summary to display on homepage.
summary = "Site servant à publier la méthodologie et les résultats du projet."

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "Mapa_de_las_lagunas_rios_y_lugares_que_circundan_a_Mexico_300px.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["R","Mexique", "Website", "Site Internet"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
#image = "headers/bubbles-wide.jpg"
#caption = "My caption :smile:"
image = ""
caption = ""
+++

Un des livrables attendus était la publication des résultats sur internet. Le site que vous êtes en train d'explorer est la réponse à cette attente.
Il a été généré à l'aide de  [*blogdown*](https://bookdown.org/yihui/blogdown/). 
