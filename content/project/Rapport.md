+++
# Date this page was created.
date = "2017-01-01"

# Project title.
title = "Rapport"

# Project summary to display on homepage.
summary = "Mémoire présentant la méthodologie et les résultats obtenus."

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "Mapa_de_las_lagunas_rios_y_lugares_que_circundan_a_Mexico_300px.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["R","Mexique", "Rapport"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
#image = "headers/bubbles-wide.jpg"
#caption = "My caption :smile:"
image = ""
caption = ""
+++

Un des livrables attendus est la production d'un mémoire présentant la méthodologie employée et les résultats obtenus. 
Le document pdf ainsi que les sources sont disponibles à cet endroit:



