+++
# About/Biography widget.
widget = "about"
active = true
date = "2018-01-01T00:00:00"

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Analyse de données",
    "Géographie",
    "Mexique"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Master 2 Géomatique, Géodécisionnel, Géomarketing et Multimédia"
  institution = "Université Paris8"
  year = 2017
 
+++

# Description du projet

Ekaterina, Mathieu et Nicolas sont 3 étudiants en master 2 Géomatique, Géodécisionnel, Géomarketing et Multimédia au sein du département de Géographie de l'Université Paris 8. Dans le cadre d'un projet transversal mettant en pratique les outils d'analyse de données vus en cours de formation, il est demandé aux étudiants de réaliser une étude sur un sujet déterminé. Cela afin de les inciter à manipuler des jeux de données réels (parfois volumineux), toutefois le choix des outils mis en oeuvre est libre. Pour ce projet, les étudiants ont, pour diverses raisons (licences, possibilités techniques, appétance), choisi de travailler majoritairement avec le language de programmation [R](https://fr.wikipedia.org/wiki/R_(langage)) et les outils qu'il propose. 
